#
# Build Stage
#
FROM maven:3.6.3-jdk-8-slim AS build
COPY src /home/pablo/Documentos/dev/notas/src
COPY pom.xml /home/pablo/Documentos/dev/notas
RUN mvn -f /home/pablo/Documentos/dev/notas/pom.xml clean package

#
# Package Stage
#

FROM openjdk:8-jre-slim
COPY --from=build /home/pablo/Documentos/dev/notas/target/nota-fiscal-0.0.1-SNAPSHOT.jar /usr/local/lib/demo.jar
EXPOSE 9091
ENTRYPOINT ["java","-jar","/usr/local/lib/demo.jar"]