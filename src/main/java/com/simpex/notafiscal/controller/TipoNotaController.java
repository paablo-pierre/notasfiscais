package com.simpex.notafiscal.controller;

import com.simpex.notafiscal.DTO.TipoNotaDTO;
import com.simpex.notafiscal.model.entity.TipoNota;
import com.simpex.notafiscal.service.TipoNotaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Api("Tipo de Nota")
@RequestMapping(value = "/tipos")
public class TipoNotaController {

    TipoNotaService tipoNotaService;

    @Autowired
    public TipoNotaController(TipoNotaService tipoNotaService) {
        this.tipoNotaService = tipoNotaService;
    }

    @ApiOperation(value = "Busca todas os tipos de notas")
    @GetMapping
    public ResponseEntity<List<TipoNota>> buscarTipos() {
        return ResponseEntity.ok().body(tipoNotaService.findAllNotes());
    }

    @ApiOperation(value = "Insere uma nova categoria de nota fiscal")
    @PostMapping
    public ResponseEntity<TipoNotaDTO> salvarTipo(@RequestBody @Valid TipoNotaDTO tipoNotaDTO) {

        TipoNotaDTO nota = tipoNotaService.salvar(tipoNotaDTO);


        return ResponseEntity.ok().body(nota);
    }

}
