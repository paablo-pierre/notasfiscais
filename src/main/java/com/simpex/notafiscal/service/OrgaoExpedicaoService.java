package com.simpex.notafiscal.service;

import com.simpex.notafiscal.DTO.OrgaoExpedicaoDTO;
import com.simpex.notafiscal.model.entity.OrgaoExpedicao;

import java.util.List;

public interface OrgaoExpedicaoService {

    List<OrgaoExpedicaoDTO> findAllOrgaos();

    List<OrgaoExpedicao> findOrgaosByNames();

    OrgaoExpedicaoDTO findOrgaoByName(String nome);

    OrgaoExpedicaoDTO findById(Long id);
}
