package com.simpex.notafiscal.service.impl;

import com.simpex.notafiscal.DTO.OrgaoExpedicaoDTO;
import com.simpex.notafiscal.model.entity.OrgaoExpedicao;
import com.simpex.notafiscal.model.repository.OrgaoExpedicaoRepository;
import com.simpex.notafiscal.service.OrgaoExpedicaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service
public class OrgaoExpedicaoServiceImpl implements OrgaoExpedicaoService {

    @Autowired
    OrgaoExpedicaoRepository orgaoExpedicaoRepository;

    OrgaoExpedicaoDTO orgaoExpedicaoDTO = new OrgaoExpedicaoDTO();

    @Override
    public List<OrgaoExpedicaoDTO> findAllOrgaos() {

        return null;
    }

    @Override
    public List<OrgaoExpedicao> findOrgaosByNames() {
        return null;
    }

    @Override
    public OrgaoExpedicaoDTO findOrgaoByName(String nome) {
        return null;
    }

    @Override
    public OrgaoExpedicaoDTO findById(Long id) {
        return null;
    }
}
