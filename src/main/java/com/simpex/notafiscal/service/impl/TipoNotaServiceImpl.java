package com.simpex.notafiscal.service.impl;

import com.simpex.notafiscal.DTO.TipoNotaDTO;
import com.simpex.notafiscal.converter.TipoNotaConverter;
import com.simpex.notafiscal.model.entity.TipoNota;
import com.simpex.notafiscal.model.repository.TipoNotaRepository;
import com.simpex.notafiscal.service.TipoNotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TipoNotaServiceImpl implements TipoNotaService {

    TipoNotaRepository tipoNotaRepository;
    TipoNotaConverter tipoNotaConverter = new TipoNotaConverter();

    @Autowired
    public TipoNotaServiceImpl(TipoNotaRepository tipoNotaRepository) {
        this.tipoNotaRepository = tipoNotaRepository;
    }

    @Override
    public List<TipoNota> findAllNotes() {
        return tipoNotaRepository.findAll();
    }

    @Override
    public Optional<TipoNota> findNotaById(Long id) {
        return tipoNotaRepository.findById(id);
    }

    @Override
    public TipoNotaDTO salvar(TipoNotaDTO tipoNota) {

        TipoNota nota = tipoNotaConverter.converterDtoParaTipoNota(tipoNota);
        nota = tipoNotaRepository.save(nota);

        return tipoNotaConverter.conveterObjParaDto(nota);
    }
}
