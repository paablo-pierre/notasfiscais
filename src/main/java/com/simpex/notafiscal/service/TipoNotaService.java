package com.simpex.notafiscal.service;

import com.simpex.notafiscal.DTO.TipoNotaDTO;
import com.simpex.notafiscal.model.entity.TipoNota;

import java.util.List;
import java.util.Optional;

public interface TipoNotaService {

    List<TipoNota> findAllNotes();

    Optional<TipoNota> findNotaById(Long id);

    TipoNotaDTO salvar(TipoNotaDTO tipoNota);
}
