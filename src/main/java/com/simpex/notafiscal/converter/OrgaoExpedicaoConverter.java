package com.simpex.notafiscal.converter;

import com.simpex.notafiscal.DTO.OrgaoExpedicaoDTO;
import com.simpex.notafiscal.model.entity.OrgaoExpedicao;

public class OrgaoExpedicaoConverter {

    public OrgaoExpedicaoDTO converterObjetoParaDto(OrgaoExpedicao orgaoExpedicao) {
        return null;
    }

    public OrgaoExpedicao converterDtoParaObjeto(OrgaoExpedicaoDTO orgaoExpedicaoDTO){
        return new OrgaoExpedicao(orgaoExpedicaoDTO.getNomeOrgao(), orgaoExpedicaoDTO.getMunicipio());
    }
}
