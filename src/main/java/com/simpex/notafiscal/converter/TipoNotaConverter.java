package com.simpex.notafiscal.converter;

import com.simpex.notafiscal.DTO.TipoNotaDTO;
import com.simpex.notafiscal.model.entity.TipoNota;

import java.util.Objects;

public class TipoNotaConverter {

    TipoNota tipoNota = new TipoNota();
    TipoNotaDTO tipoNotaDTO = new TipoNotaDTO();

    public TipoNota converterDtoParaTipoNota(TipoNotaDTO tipoNotaDTO) {
        if(Objects.nonNull(tipoNotaDTO)) {
            tipoNota.setDescricao(tipoNotaDTO.getDescricao());
        }
        return tipoNota;
    }

    public TipoNotaDTO conveterObjParaDto(TipoNota tipoNota) {
        if(Objects.nonNull(tipoNota)) {
            tipoNotaDTO.setDescricao(tipoNota.getDescricao());
        }
        return tipoNotaDTO;
    }
}
