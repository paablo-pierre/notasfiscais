package com.simpex.notafiscal.DTO;

import lombok.Data;

@Data
public class OrgaoExpedicaoDTO {

    private String nomeOrgao;
    private String municipio;
}
