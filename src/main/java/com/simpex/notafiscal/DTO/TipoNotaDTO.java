package com.simpex.notafiscal.DTO;

import com.simpex.notafiscal.model.entity.TipoNota;
import lombok.Data;

@Data
public class TipoNotaDTO {
    private String descricao;
}