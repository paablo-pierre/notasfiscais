package com.simpex.notafiscal.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "TB03_NOTA_FISCAL")
@SequenceGenerator(name = "TB03_NOTA_FISCAL_SEQ", sequenceName = "TB03_NOTA_FISCAL_SEQ", allocationSize = 1)
@Data
@EqualsAndHashCode
public class NotaFiscal implements Serializable {

    private static final long serialVersionUID = 7013635562020885857L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB03_NOTA_FISCAL_SEQ")
    @Column(name = "PK_IDT_NOTA_FISCAL")
    private Long id;

    @Column(name = "VALOR_NOTA")
    private BigDecimal valorNota;

    @JoinColumn(name = "FK_IDT_TIPO_NOTA")
    @ManyToOne
    private TipoNota tipoNota;

    @JoinColumn(name = "FK_IDT_ORGAO_EXPEDICAO")
    @ManyToOne
    private OrgaoExpedicao orgaoExpedicao;
}
