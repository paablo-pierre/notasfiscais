package com.simpex.notafiscal.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "TB02_TIPO_NOTA")
@SequenceGenerator(name = "TB02_TIPO_NOTA_SEQ", sequenceName = "TB02_TIPO_NOTA_SEQ", allocationSize = 1)
@Data @EqualsAndHashCode
@AllArgsConstructor @NoArgsConstructor
public class TipoNota implements Serializable {

    private static final long serialVersionUID = -6753794039228185835L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB02_TIPO_NOTA_SEQ")
    @Column(name = "PK_IDT_TIPO_NOTA")
    private Long id;

    @Column(name = "DES_TIPO_NOTA")
    @NotNull(message = "Descriçao e necessario")
    @NotBlank(message = "Descriçao nao pode estar vazia")
    @Length(min = 3, max = 100)
    private String descricao;

}
