package com.simpex.notafiscal.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TB01_ORGAO_EXPEDICAO")
@SequenceGenerator(name = "TB01_ORGAO_EXPEDICAO_SEQ", sequenceName = "TB01_ORGAO_EXPEDICAO_SEQ", allocationSize = 1)
@Data
@EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor
public class OrgaoExpedicao implements Serializable {

    private static final long serialVersionUID = -4361509358498061676L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB01_ORGAO_EXPEDICAO_SEQ")
    @Column(name = "PK_IDT_ORGAO")
    private Long id;

    @Column(name = "DES_NAME_ORGAO")
    private String nomeOrgao;

    @Column(name = "DES_NAME_MUNICIPIO")
    private String nomeMunicipio;

    public OrgaoExpedicao(String nomeOrgao, String municipio) {
        this.nomeOrgao = nomeOrgao;
        this.nomeMunicipio = municipio;
    }
}
