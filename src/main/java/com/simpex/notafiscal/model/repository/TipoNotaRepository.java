package com.simpex.notafiscal.model.repository;

import com.simpex.notafiscal.model.entity.TipoNota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoNotaRepository extends JpaRepository<TipoNota, Long> {
}
