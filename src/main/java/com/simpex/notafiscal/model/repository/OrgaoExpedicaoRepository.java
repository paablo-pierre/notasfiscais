package com.simpex.notafiscal.model.repository;

import com.simpex.notafiscal.model.entity.OrgaoExpedicao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrgaoExpedicaoRepository extends JpaRepository<OrgaoExpedicao, Long> {
}
